package com.devcamp.project.realestate.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.devcamp.project.realestate.model.CAdvise;


@Repository
public interface IAdviseRepository extends JpaRepository <CAdvise,Integer>{
    @Query(value = "SELECT * FROM `advise` WHERE `status` = 'N' ;", nativeQuery = true)
    List<CAdvise> getAllAdviseFilterStatus();

    @Query(value = "SELECT * FROM `advise` WHERE `status` = 'Y' ;", nativeQuery = true)
    List<CAdvise> getAllAdviseFilterStatusY();
}