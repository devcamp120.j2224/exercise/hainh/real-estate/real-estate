package com.devcamp.project.realestate.service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.devcamp.project.realestate.model.CAdvise;
import com.devcamp.project.realestate.repository.IAdviseRepository;

@Service
public class AdviseService {
    
    @Autowired
    IAdviseRepository pAdviseRepository;

    public List<CAdvise> getAllAdvise(){
        List<CAdvise> listAdvise = new ArrayList<CAdvise>();
        pAdviseRepository.findAll().forEach(listAdvise::add);
        return listAdvise;
    }

    public Object createAdvise(CAdvise cAdvise){

        CAdvise newAdvise = new CAdvise();
        newAdvise.setHoTen(cAdvise.getHoTen());
        newAdvise.setEmail(cAdvise.getEmail());
        newAdvise.setPhone(cAdvise.getPhone());
        newAdvise.setNote(cAdvise.getNote());
        newAdvise.setStatus("N");

        CAdvise savedAdvise = pAdviseRepository.save(newAdvise);
        return savedAdvise;
    }

    public Object updateAdvise(CAdvise cAdvise,Optional<CAdvise> adviseData){
        
        CAdvise newAdvise = adviseData.get();
        newAdvise.setHoTen(cAdvise.getHoTen());
        newAdvise.setEmail(cAdvise.getEmail());
        newAdvise.setPhone(cAdvise.getPhone());
        newAdvise.setNote(cAdvise.getNote());
        newAdvise.setStatus(cAdvise.getStatus());

        CAdvise savedAdvise = pAdviseRepository.save(newAdvise);
        return savedAdvise;
    }
}
