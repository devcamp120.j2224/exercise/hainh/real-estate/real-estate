package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.IRegionLinkRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegionLinkService {
    @Autowired
    IRegionLinkRepository pRegionLinkRepository;

    public List<CRegionLink> getRegionLinkList() {
        List<CRegionLink> regionLinkList = new ArrayList<CRegionLink>();
        pRegionLinkRepository.findAll().forEach(regionLinkList::add);
        return regionLinkList;
    }

    public Object createRegionLinkObj(CRegionLink cRegionLink) {

        CRegionLink newRegionLink = new CRegionLink();
        newRegionLink.setName(cRegionLink.getName());
        newRegionLink.setDescription(cRegionLink.getDescription());
        newRegionLink.setPhoto(cRegionLink.getPhoto());
        newRegionLink.setAddress(cRegionLink.getAddress());
        newRegionLink.setLat(cRegionLink.getLat());
        newRegionLink.setLng(cRegionLink.getLng());

        CRegionLink savedRegionLink = pRegionLinkRepository.save(newRegionLink);
        return savedRegionLink;
    }

    public Object updateRegionLinkObj(CRegionLink cRegionLink, Optional<CRegionLink> cRegionLinkData) {

        CRegionLink newRegionLink = cRegionLinkData.get();
        newRegionLink.setName(cRegionLink.getName());
        newRegionLink.setDescription(cRegionLink.getDescription());
        newRegionLink.setPhoto(cRegionLink.getPhoto());
        newRegionLink.setAddress(cRegionLink.getAddress());
        newRegionLink.setLat(cRegionLink.getLat());
        newRegionLink.setLng(cRegionLink.getLng());

        CRegionLink savedRegionLink = pRegionLinkRepository.save(newRegionLink);
        return savedRegionLink;
    }
}
