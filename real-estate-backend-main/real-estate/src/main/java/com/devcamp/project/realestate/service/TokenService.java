package com.devcamp.project.realestate.service;

import com.devcamp.project.realestate.entity.Token;

public interface TokenService {

    Token createToken(Token token);

    Token findByToken(String token);
}
