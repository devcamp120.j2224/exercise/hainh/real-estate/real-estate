package com.devcamp.project.realestate.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.project.realestate.model.CEmployees;
import com.devcamp.project.realestate.repository.IEmployeeRepository;
import com.devcamp.project.realestate.security.UserPrincipal;
import com.devcamp.project.realestate.service.UserService;

import java.util.HashSet;
import java.util.Set;

@Service
public class EmployeeServiceImpl implements UserService {

    @Autowired
    private IEmployeeRepository pEmployeeRepository;

    @Override
    public CEmployees createCEmployees(CEmployees user) {
        return pEmployeeRepository.saveAndFlush(user);
    }

    @Override
    public UserPrincipal findByUsername(String username) {
        CEmployees user = pEmployeeRepository.findByUsername(username);
        UserPrincipal userPrincipal = new UserPrincipal();
        if (null != user) {
            Set<String> authorities = new HashSet<>();
            if (null != user.getRoles())
                user.getRoles().forEach(r -> {
                    authorities.add(r.getRoleKey());
                });

            userPrincipal.setUserId(user.getEmployeeID());
            userPrincipal.setUsername(user.getUsername());
            userPrincipal.setPassword(user.getPassword());
            userPrincipal.setAuthorities(authorities);
        }
        return userPrincipal;
    }

}
