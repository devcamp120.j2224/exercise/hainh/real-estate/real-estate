package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.MasterLayoutService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class MasterLayoutController {
    @Autowired
    IProjectRepository pProjectRepository;

    @Autowired
    IMasterLayoutRepository pMasterLayoutRepository;

    @Autowired
    MasterLayoutService pMasterLayoutService;

    @GetMapping("/masterLayouts")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getMasterLayoutList() {
        if (!pMasterLayoutService.getMasterLayoutList().isEmpty()) {
            return new ResponseEntity<>(pMasterLayoutService.getMasterLayoutList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/masterLayouts/{masterLayoutId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getMasterLayoutById(@PathVariable Integer masterLayoutId) {
        if (pMasterLayoutRepository.findById(masterLayoutId).isPresent()) {
            return new ResponseEntity<>(pMasterLayoutRepository.findById(masterLayoutId).get(),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/masterLayouts/project/{projectId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createMasterLayout(@PathVariable Integer projectId, @RequestBody CMasterLayout cMasterLayout) {
        try {
            Optional<CProject> projectData = pProjectRepository.findById(projectId);
            if (projectData.isPresent()) {
                return new ResponseEntity<>(pMasterLayoutService.createMasterLayoutObj(cMasterLayout, projectData), HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified MasterLayout: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/masterLayouts/{masterLayoutId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateMasterLayout(@PathVariable("masterLayoutId") Integer masterLayoutId,
            @RequestBody CMasterLayout cMasterLayout) {
        try {
            Optional<CMasterLayout> masterLayoutData = pMasterLayoutRepository.findById(masterLayoutId);
            if (masterLayoutData.isPresent()) {
                return new ResponseEntity<>(pMasterLayoutService.updateMasterLayoutObj(cMasterLayout, masterLayoutData), HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified MasterLayout: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/masterLayouts/{masterLayoutId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteMasterLayoutById(@PathVariable("masterLayoutId") int masterLayoutId) {
        try {
            pMasterLayoutRepository.deleteById(masterLayoutId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
