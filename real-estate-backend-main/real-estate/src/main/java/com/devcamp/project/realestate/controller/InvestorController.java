package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.InvestorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class InvestorController {
    @Autowired
    IInvestorRepository pInvestorRepository;

    @Autowired
    InvestorService pInvestorService;

    @GetMapping("/investors")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getInvestorList() {
        if (!pInvestorService.getInvestorList().isEmpty()) {
            return new ResponseEntity<>(pInvestorService.getInvestorList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/investors/{investorId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getInvestorById(@PathVariable Integer investorId) {
        if (pInvestorRepository.findById(investorId).isPresent()) {
            return new ResponseEntity<>(pInvestorRepository.findById(investorId).get(),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/investors")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createInvestor(@RequestBody CInvestor cInvestor) {
        try {
            return new ResponseEntity<>(pInvestorService.createInvestorObj(cInvestor), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Investor: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/investors/{investorId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateInvestor(@PathVariable("investorId") Integer investorId,
            @RequestBody CInvestor cInvestor) {
        try {
            Optional<CInvestor> investorData = pInvestorRepository.findById(investorId);
            if (investorData.isPresent()) {
                return new ResponseEntity<>(pInvestorService.updateInvestorObj(cInvestor, investorData), HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Investor: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/investors/{investorId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteInvestorById(@PathVariable("investorId") int investorId) {
        try {
            pInvestorRepository.deleteById(investorId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
