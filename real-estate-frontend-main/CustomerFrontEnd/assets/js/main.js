
(function() {
  "use strict";

  /**
   * Easy selector helper function
   */
  const select = (el, all = false) => {
    el = el.trim()
    if (all) {
      return [...document.querySelectorAll(el)]
    } else {
      return document.querySelector(el)
    }
  }

  /**
   * Easy event listener function
   */
  const on = (type, el, listener, all = false) => {
    let selectEl = select(el, all)
    if (selectEl) {
      if (all) {
        selectEl.forEach(e => e.addEventListener(type, listener))
      } else {
        selectEl.addEventListener(type, listener)
      }
    }
  }

  /**
   * Easy on scroll event listener 
   */
  const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener)
  }

  /**
   * Toggle .navbar-reduce
   */
  let selectHNavbar = select('.navbar-default')
  if (selectHNavbar) {
    onscroll(document, () => {
      if (window.scrollY > 100) {
        selectHNavbar.classList.add('navbar-reduce')
        selectHNavbar.classList.remove('navbar-trans')
      } else {
        selectHNavbar.classList.remove('navbar-reduce')
        selectHNavbar.classList.add('navbar-trans')
      }
    })
  }

  /**
   * Back to top button
   */
  let backtotop = select('.back-to-top')
  if (backtotop) {
    const toggleBacktotop = () => {
      if (window.scrollY > 100) {
        backtotop.classList.add('active')
      } else {
        backtotop.classList.remove('active')
      }
    }
    window.addEventListener('load', toggleBacktotop)
    onscroll(document, toggleBacktotop)
  }

  /**
   * Preloader
   */
  let preloader = select('#preloader');
  if (preloader) {
    window.addEventListener('load', () => {
      preloader.remove()
    });
  }

  /**
   * Search window open/close
   */
  let body = select('body');
  on('click', '.navbar-toggle-box', function(e) {
    e.preventDefault()
    body.classList.add('box-collapse-open')
    body.classList.remove('box-collapse-closed')
  })

  on('click', '.close-box-collapse', function(e) {
    e.preventDefault()
    body.classList.remove('box-collapse-open')
    body.classList.add('box-collapse-closed')
  })

  /**
   * Intro Carousel
   */
  new Swiper('.intro-carousel', {
    speed: 600,
    loop: true,
    autoplay: {
      delay: 2000,
      disableOnInteraction: false
    },
    slidesPerView: 'auto',
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    }
  });

  /**
   * Property carousel
   */
  new Swiper('#property-carousel', {
    speed: 600,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    slidesPerView: 'auto',
    pagination: {
      el: '.propery-carousel-pagination',
      type: 'bullets',
      clickable: true
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },

      1200: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    }
  });

  /**
   * News carousel
   */
  new Swiper('#news-carousel', {
    speed: 600,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    slidesPerView: 'auto',
    pagination: {
      el: '.news-carousel-pagination',
      type: 'bullets',
      clickable: true
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },

      1200: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    }
  });

  /**
   * Testimonial carousel
   */
  new Swiper('#testimonial-carousel', {
    speed: 600,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    slidesPerView: 'auto',
    pagination: {
      el: '.testimonial-carousel-pagination',
      type: 'bullets',
      clickable: true
    }
  });

  /**
   * Property Single carousel
   */
  new Swiper('#property-single-carousel', {
    speed: 600,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    pagination: {
      el: '.property-single-carousel-pagination',
      type: 'bullets',
      clickable: true
    }
  });

})()

$(document).ready(function() {


  const token = getCookie("token");

  var gUserId;
  var gTotalPage;
  const pg = document.getElementById("pagination");
  const curPage = document.getElementById("curpage");

  if (token != "") {
    $("#btn-log-in").hide();

  } else {
    $("#btn-log-out").hide();
    $("#btn-filter-your-post").hide();
  }
  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  callApiGetTotalPages();

  const valuePage = {
    truncate: true,
    curPage: 1,
    numLinksTwoSide: 1,
    totalPages: gTotalPage
  };

  onPageLoading(); 

  pg.onclick = (e) => {
    const ele = e.target;
    if (ele.dataset.page) {
      const pageNumber = parseInt(e.target.dataset.page, 10);

      valuePage.curPage = pageNumber;
      curPage.value = pageNumber;
      pagination(valuePage);
      callApiGetProject(pageNumber - 1);
      $('html, body').animate({
      scrollTop: $("#property-content").offset().top
    }, 1000);
    }
  };

  $(".to-slideshow-example").click(function () {
    $('html, body').animate({
      scrollTop: $("#slideshow-example").offset().top
    }, 1000);
  });

  $(".to-property-content").click(function () {
    $('html, body').animate({
      scrollTop: $("#property-content").offset().top
    }, 1000);
  });

  $(".to-new-post").click(function () {
    $('html, body').animate({
      scrollTop: $("#new-post").offset().top
    }, 1000);
  });

  $(".to-contact-us").click(function () {
    $('html, body').animate({
      scrollTop: $("#contact-us").offset().top
    }, 1000);
  });

  $(".to-utility").click(function () {
    $('html, body').animate({
      scrollTop: $("#utility").offset().top
    }, 1000);
  });

  //Set sự kiện cho nút login
  $("#btn-log-in").on("click", function () {
    window.location.href = "../AdminFrontEnd/index.html";
  });

  //Set sự kiện cho nút logout
  $("#btn-log-out").on("click", function () {
    redirectToLogin();
  });

  //Set sự kiện cho nút đăng tin
  $("#btn-new-post").on("click", function () {
    if (token != "") {
      window.location.href = "add-realestate.html";
    } else {
      window.location.href = "../AdminFrontEnd/index.html";
    }
  });

  // gán xự kiện lấy data cho nút sửa tin
  $("#current-real-estate").on("click", ".btn-update-post", function () {
    if (token != "") {
      onBtnUpdateRealEstateClick(this);
    } else {
      window.location.href = "../AdminFrontEnd/index.html";
    }
  });

  // gán xự kiện lấy data của project
  $("#real-estate-project").on("click", ".project-detail", function () {
    onCardProjectDetailClick(this);
  });

  // gán xự kiện lấy data của project
  $("#real-estate-project").on("click", ".project-detail1", function () {
    onCardProjectDetail1Click(this);
  });

  // gán xự kiện lấy data của realEstate
  $("#current-real-estate").on("click", ".detail-real-estate", function () {
    onDetailRealEstateClick(this);
  });

  // gán xự kiện lấy data của realEstate
  $("#current-real-estate").on("click", ".detail-real-estate1", function () {
    onDetailRealEstate1Click(this);
  });

  // gán sự kiện lọc dữ liệu bài đăng của user
  $("#btn-filter-your-post").on("click", function () {
    callApiGetRealEstateByEmployeeId();
  });

  // gán xự kiện khi ô select thay đổi
  $("#select-province").on("change", function () {
    $("#select-district").html("");
    var bProvinceId = $(this).val();
    callApiGetDistrictByProvinceId(bProvinceId);
  });

  // gán xự kiện  nút tìm kiếm realEstate
  $("#btn-find-project").on("click", function () {
    $("#pagination").hide();
    onBtnFindProjectClick();
    $('html, body').animate({
      scrollTop: $("#property-content").offset().top
    }, 1000);
  });



  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function onPageLoading() {
    "use strict";
    callApiGetProject(0);

    callApiGetAllProvinces();

    callApiGetCurrentListRealEstate();

    callApiGetUserIdByToken();

    pagination(valuePage);
  }

  function onCardProjectDetailClick(paramDetail) {
    "use strict";
    // B1: Thu thập dữ liệu
    var vValueId = $(paramDetail).children('input').val();
    //console.log(vValueId);
    // B2: Kiểm tra dữ liệu (bỏ qua)
    // B3: xử lý hiển thị dữ liệu
    const vPROJECT_DETAILS_URL = "project-details.html"
    var vUrlSiteToOpen = vPROJECT_DETAILS_URL + "?" + "projectId=" + vValueId;
    window.location.href = vUrlSiteToOpen;
  }

  function onCardProjectDetail1Click(paramDetail) {
    "use strict";
    // B1: Thu thập dữ liệu
    var vValueId = $(paramDetail).children('input').val();
    //console.log(vValueId);
    // B2: Kiểm tra dữ liệu (bỏ qua)
    // B3: xử lý hiển thị dữ liệu
    const vPROJECT_DETAILS_URL = "project-details.html"
    var vUrlSiteToOpen = vPROJECT_DETAILS_URL + "?" + "projectId=" + vValueId;
    window.location.href = vUrlSiteToOpen;
  }

  function onDetailRealEstateClick(paramDetail) {
    "use strict";
    // B1: Thu thập dữ liệu
    var vValue = $(paramDetail).children('input').val();
    //console.log(vValue);
    // B2: Kiểm tra dữ liệu (bỏ qua)
    // B3: xử lý hiển thị dữ liệu
    const vPROPERTIES_DETAILS_URL = "properties-details.html"
    var vUrlSiteToOpen = vPROPERTIES_DETAILS_URL + "?" + "realEstateId=" + vValue;
    window.location.href = vUrlSiteToOpen;
  }

  function onDetailRealEstate1Click(paramDetail) {
    "use strict";
    // B1: Thu thập dữ liệu
    var vValue = $(paramDetail).children('input').val();
    //console.log(vValue);
    // B2: Kiểm tra dữ liệu (bỏ qua)
    // B3: xử lý hiển thị dữ liệu
    const vPROPERTIES_DETAILS_URL = "properties-details.html"
    var vUrlSiteToOpen = vPROPERTIES_DETAILS_URL + "?" + "realEstateId=" + vValue;
    window.location.href = vUrlSiteToOpen;
  }

  function onBtnUpdateRealEstateClick(paramDetail) {
    "use strict";
    // B1: Thu thập dữ liệu
    var vValue = $(paramDetail).children('input').val();
    //console.log(vValue);
    // B2: Kiểm tra dữ liệu(bỏ qua)
    // B3: xử lý hiển thị dữ liệu
    callApiGetRealEstateById(vValue);
  }

  function onBtnFindProjectClick() {
    "use strict";
    // B1: Thu thập dữ liệu
    var vProvinceSelected = $("#select-province").val();
    var vDistrictSelected = $("#select-district").val();
    // B2: Kiểm tra dữ liệu (bỏ qua)
    // B3: xử lý hiển thị dữ liệu
    callApiGetProjectByProvinceIdAndDistrictId(vProvinceSelected, vDistrictSelected);

  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  function callApiGetProject(paramPageNumber) {
    $.ajax({
      url: "http://localhost:8080/projectsList/page/" + paramPageNumber,
      method: "GET",
      async: "false",
      success: function (paramResponseObj) {
        //console.log(paramResponseObj);
        getProjectDataLoadToForm(paramResponseObj);
      },
      error: function (paramError) {
        //console.log(paramError);
      }
    });
  }

  // Hàm gọi api tất cả tỉnh thành
  function callApiGetAllProvinces() {
    $.ajax({
      url: "http://localhost:8080/provinces",
      method: "GET",
      async: false,
      success: function (paramResponseObj) {
        //console.log(paramResponseObj);
        loadDataToProvinceSelect(paramResponseObj);
      },
      error: function (paramError) {
        console.log(paramError);
      }
    });
  }

  // Hàm xử lý gọi api lấy dữ liệu district bằng province id
  function callApiGetDistrictByProvinceId(paramProvinceId) {
    "use strict";
    $.ajax({
      url: "http://localhost:8080/districts/province/" + paramProvinceId,
      method: "GET",
      async: false,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        "Authorization": "Token " + token,
      },
      success: function (paramResponseObj) {
        //console.log(paramResponseObj);
        loadDataToDistrictSelect(paramResponseObj);
      },
      error: function (paramError) {
        console.log(paramError);
      }
    });
  }

  // Hàm xử lý gọi api lấy dữ liệu realestate bằng provinceid và districtid
  function callApiGetProjectByProvinceIdAndDistrictId(paramProvinceId, paramDistrictId) {
    "use strict";
    $.ajax({
      url: "http://localhost:8080/projects/" + paramProvinceId + "/" + paramDistrictId,
      method: "GET",
      async: false,
      success: function (paramResponseObj) {
        //console.log(paramResponseObj);
        getProjectDataLoadToForm(paramResponseObj);
      },
      error: function (paramError) {
        console.log(paramError);
        alertError("Không tìm thấy dự án BĐS nào.");
      }
    });
  }

  // Hàm xử lý gọi api lấy dữ liệu realestate bằng provinceid và districtid
  function callApiGetCurrentListRealEstate() {
    "use strict";
    $.ajax({
      url: "http://localhost:8080/currentListRealEstates",
      method: "GET",
      async: false,
      success: function (paramResponseObj) {
        //console.log(paramResponseObj);
        getRealEstateDataLoadToForm(paramResponseObj);
      },
      error: function (paramError) {
        console.log(paramError);
      }
    });
  }

  function callApiGetUserIdByToken() {
    $.ajax({
      url: "http://localhost:8080/findUserId/Token",
      type: 'GET',
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      headers: {
        "Authorization": "Token " + token,
      },
      success: function (responseObject) {
        // console.log(responseObject);
        callApiGetEmployeeByID(responseObject);
        gUserId = responseObject;
        
      },
      error: function (pAjaxContext) {
        //redirectToLogin();
      }
    });
  };

  // Hàm gọi api lấy dữ liệu realEstate bằng id
  function callApiGetRealEstateByEmployeeId() {
    $.ajax({
      url: "http://localhost:8080/realEstates/limit/employee/" + gUserId,
      method: "GET",
      async: false,
      headers: {
        "Authorization": "Token " + token,
      },
      success: function (paramResponseObj) {
        console.log(paramResponseObj);
        getRealEstateDataLoadToForm(paramResponseObj);
      },
      error: function (paramError) {
        console.log(paramError);
      }
    });
  }

  function callApiGetRealEstateById(paramRealEstateId) {
    $.ajax({
      url: "http://localhost:8080/realEstates/" + paramRealEstateId,
      type: 'GET',
      dataType: "json",
      success: function (responseObject) {
        //console.log(responseObject);
        if (gUserId == responseObject.employee.employeeID) {
          const vUPDATE_REALESTATE_URL = "update-realestate.html"
          var vUrlSiteToOpen = vUPDATE_REALESTATE_URL + "?" + "realEstateId=" + paramRealEstateId;
          window.location.href = vUrlSiteToOpen;
        } else {
          alertError("Không phải Tin/Bài đăng của bạn.");
        }
      },
      error: function (pAjaxContext) {
        console.assert(pAjaxContext);
      }
    });
  };

  // Hàm gọi api lậy tổng số trang dự án
  function callApiGetTotalPages() {
    $.ajax({
      url: "http://localhost:8080/totalPageNumberProjects",
      method: "GET",
      async: false,
      success: function (paramResponseObj) {
        //console.log(paramResponseObj);
        gTotalPage = paramResponseObj;
      },
      error: function (paramError) {
        //console.log(paramError);
      }
    });
  };

  // Load dữ liệu vào ô select trên modal create ward
  function loadDataToProvinceSelect(paramResponse) {
    "use strict";
    var vProvinceSelect = $("#select-province");
    for (let i = 0; i < paramResponse.length; i++) {
      var bOption = $("<option/>");
      bOption.prop("value", paramResponse[i].id);
      bOption.prop("text", paramResponse[i].name);
      vProvinceSelect.append(bOption);
    };
  }

  // Load dữ liệu phản hồi vào ô select
  function loadDataToDistrictSelect(paramResDistrictList) {
    "use strict";
    var vDistrictSelectElement = $("#select-district");
    if (paramResDistrictList.length > 0) {
      vDistrictSelectElement.prop("disabled", false);

      for (let i = 0; i < paramResDistrictList.length; i++) {
        var bDistrictOption = $("<option/>");
        bDistrictOption.prop("value", paramResDistrictList[i].id);
        bDistrictOption.prop("text", paramResDistrictList[i].name);
        vDistrictSelectElement.append(bDistrictOption);
      };
    } else {
      vDistrictSelectElement.prop("disabled", true);
    }
  }

  // Hàm lấy danh sách các realestate
  function getProjectDataLoadToForm(paramProjectList) {
    "use strict";
    $("#real-estate-project").html("");
    var vRealEstate = "";
    for (let bI = 0; bI < paramProjectList.length; bI++) {
      vRealEstate += `
      <div class="col-lg-4 card-group">
      <div class="card card-hover mb-4">
        <!-- Card Header-->
        <div class="card-img-top">
          <a class="project-detail" href="#">
            <input type="hidden" value="` + paramProjectList[bI].id + `">
            <img src="` + paramProjectList[bI].photo + `" class="rounded-top card-img-top">
          </a>
        </div>
        <!-- Card Body-->
        <div class="card-body">
          <div class="mt-2">
            <a class="project-detail1" href="#">
              <input type="hidden" value="` + paramProjectList[bI].id + `">
              <h4>` + paramProjectList[bI].name + `</h4>
            </a>
            <a class="mt-2 text-dark">Address: ` + paramProjectList[bI].address + `</a><br>
            <label class="mt-2">Tỉnh thành: ` + paramProjectList[bI]._district._province.name + `</label>
          </div>
          <ul class="mt-3 list-inline">
            <li class="list-inline-item border-dark border-right pr-3"><i class="fa-regular fa-square"></i>
              ` + paramProjectList[bI].acreage + `m2</li>
            <li class="list-inline-item border-dark border-right pr-3 pl-2"><i class="fa-solid fa-city"></i> ` + paramProjectList[bI].numApartment + `</li>
            <li class="list-inline-item pr-3 pl-2"><i class="fa-solid fa-building"></i> ` + paramProjectList[bI].numBlock + `</li>
          </ul>
        </div>
      </div>
    </div>`;
    $("#real-estate-project").html(vRealEstate);
    }
  }

  // Hàm lấy danh sách các realestate
  function getRealEstateDataLoadToForm(paramRealEstate) {
    "use strict";
    $("#current-real-estate").html("");
    var vRealEstate = "";
    for (let bI = 0; bI < paramRealEstate.length; bI++) {
      vRealEstate += `
      <div class="col-lg-4 card-group">
        <div class="card card-hover mb-4">
          <!-- Card Header-->
          <div class="card-img-top">
            <a class="detail-real-estate" href="#">
              <input type="hidden" value="` + paramRealEstate[bI].id + `">
              <img src="` + paramRealEstate[bI].photo + `" class="rounded-top card-img-top">
            </a>
          </div>
          <!-- Card Body-->
          <div class="card-body">
            <div class="mt-2">
              <a class="detail-real-estate1" href="#">
              <input type="hidden" value="` + paramRealEstate[bI].id + `">
                <h4>` + paramRealEstate[bI].apartCode + `</h4>
              </a>
              <p class="mt-2 text-dark">Địa chỉ: ` + paramRealEstate[bI].address + `</p>
              <p class="mt-2">Tạo bởi: ` + paramRealEstate[bI].employee.firstName + " " + paramRealEstate[bI].employee.lastName + `</p>
              <p class="mt-2">Added: ` + new Date(paramRealEstate[bI].dateCreate).toLocaleString() + `</p>
            </div>
            <ul class="mt-3 list-inline">
              <li class="list-inline-item border-dark border-right pr-3"><i class="fa-regular fa-square"></i>
                ` + paramRealEstate[bI].acreage + `m2</li>
              <li class="list-inline-item border-dark border-right pr-3 pl-2"><i class="fa-solid fa-bed"></i> ` + paramRealEstate[bI].bedroom + `</li>
              <li class="list-inline-item pr-3 pl-2"><i class="fa-solid fa-bath"></i> ` + paramRealEstate[bI].bath + `</li>
            </ul>
          </div>
          <!-- Card Footer-->
          <div class="card-footer">
            <div class="row">
              <div class="col-sm-8">
                <h4>Giá: ` + paramRealEstate[bI].price + `USD</h4>
              </div>
              <div class="col-sm-4 text-right">
                <a class="btn btn-primary btn-update-post" href="#">Sửa tin
                  <input type="hidden" value="` + paramRealEstate[bI].id + `"></a>
              </div>
            </div>
          </div>
        </div>
      </div>`;
      $("#current-real-estate").html(vRealEstate);
    }
  }

  function redirectToLogin() {
    // Trước khi logout cần xóa token đã lưu trong cookie
    setCookie("token", "", 1);
    window.location.href = "../AdminFrontEnd/index.html";
  }

  //Hàm get Cookie
  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  //Hàm setCookie đã giới thiệu ở bài trước
  function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  // Hàm alert error
  function alertError(paramTitle) {
    "use strict";
    $(function () {
      var Toast = Swal.mixin({
        toast: false,
        position: 'top',
        showConfirmButton: false,
        timer: 3500
      });
      Toast.fire({
        icon: 'error',
        title: paramTitle,
      })
    });
  }

  function pagination(paramValuePage) {
    const { totalPages, curPage, truncate, numLinksTwoSide: delta } = paramValuePage;

    const range = delta + 4; // use for handle visible number of links left side

    let render = "";
    let renderTwoSide = "";
    let dot = `<li class="pg-item"><a class="pg-link">...</a></li>`;
    let countTruncate = 0; // use for ellipsis - truncate left side or right side

    // use for truncate two side
    const numberTruncateLeft = curPage - delta;
    const numberTruncateRight = curPage + delta;

    let active = "";
    for (let pos = 1; pos <= totalPages; pos++) {
      active = pos === curPage ? "active" : "";

      // truncate
      if (totalPages >= 2 * range - 1 && truncate) {
        if (numberTruncateLeft > 3 && numberTruncateRight < totalPages - 3 + 1) {
          // truncate 2 side
          if (pos >= numberTruncateLeft && pos <= numberTruncateRight) {
            renderTwoSide += renderPage(pos, active);
          }
        } else {
          // truncate left side or right side
          if (
            (curPage < range && pos <= range) ||
            (curPage > totalPages - range && pos >= totalPages - range + 1) ||
            pos === totalPages ||
            pos === 1
          ) {
            render += renderPage(pos, active);
          } else {
            countTruncate++;
            if (countTruncate === 1) render += dot;
          }
        }
      } else {
        // not truncate
        render += renderPage(pos, active);
      }
    }

    if (renderTwoSide) {
      renderTwoSide =
        renderPage(1) + dot + renderTwoSide + dot + renderPage(totalPages);
      pg.innerHTML = renderTwoSide;
    } else {
      pg.innerHTML = render;
    }
  };

  function renderPage(index, active = "") {
    return ` <li class="pg-item ${active}" data-page="${index}">
      <a class="pg-link" href="#">${index}</a>
  </li>`;
  };

     // Hàm gọi api lấy dữ liệu employee bằng id
     function callApiGetEmployeeByID(paramEmployeeId) {
              $.ajax({
                  url: "http://localhost:8080/employees/" + paramEmployeeId,
                  method: "GET",
                  async: false,
                  success: function (paramResponseObj) {
                      console.log(paramResponseObj);
                      handlerUserDataResponse(paramResponseObj);
                  },
                  error: function (paramError) {
                      console.log(paramError);
                  }
              });
          }

          function handlerUserDataResponse(paramUserData) {
              $("#ten-dang-nhap").html(paramUserData.username);
            
          }
});